#!/bin/bash
echo -e "SETUP\t Python version and ROOT"
setupATLAS
lsetup "root 6.26.08-x86_64-centos7-gcc11-opt"

# CHANGE THIS IF NEEDED
ENV_BASE_DIR=/eos/user/v/vyazykov
ENV_NAME=myenv

# Check if the virtual environment exists
ENV_PATH=$ENV_BASE_DIR/$ENV_NAME
if [ -d "$ENV_PATH" ]; then
    echo "Virtual environment found!"
    exit 0
fi

# Create virtual environment
python3 -m venv $ENV_PATH --system-site-packages

# Activate virtual environment
source $ENV_PATH/bin/activate

# Reset the PYTHONPATH variable and add ROOT libraries to it
export PYTHONPATH=$VIRTUAL_ENV/lib64/python3.9/:$ROOTSYS/lib # not sure if it is needed

# Install required packages
python -m ensurepip --upgrade
python -m pip install --upgrade pip
pip install --upgrade numpy uproot awkward tqdm pyyaml torch torchvision torchaudio

# Deactivate virtual environment
echo "Deactivate virtual environment"
deactivate

