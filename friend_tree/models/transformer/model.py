"""
This file is providing all needed to load the model.

Needs to define the "Model" class that fulfills the following requirements:
- "__init__(self)" constructor with no arguments
- "forward(self)" method that takes an Awkward Array as input and returns a numpy array
- "self.features" property/field that returns a list of features used by the model
"""
from __future__ import annotations
import torch
import os
import awkward as ak
import numpy as np
import torch.nn as nn
import sys


class Model(nn.Module):

    def __init__(self):
        super().__init__()

        self.device = "cuda" if torch.cuda.is_available() else "cpu"

        if self.device == "cpu":
            print("WARNING: running on CPU, this will be slow!", file=sys.stderr)

        # Load the weights stored at the same directory as this file
        dirname = os.path.dirname(__file__)
        saved_data = torch.load(os.path.join(dirname, "saved_data.pt"), map_location=self.device)

        self.x_names_continuous = saved_data["x_names_continuous"]
        self.x_names_categorical = saved_data["x_names_categorical"]

        self.categorical_sizes = saved_data["categorical_sizes"]
        self.map_categorical = saved_data["map_categorical"]
        self.n_features_continuous = len(self.x_names_continuous)

        self.y_names = saved_data["y_names"]
        self.n_classes = len(self.y_names)
        self.signal_idx = self.y_names.index("ttH")

        self.mean = saved_data["mean"]
        self.std = saved_data["std"]

        self.n_blocks = saved_data["n_blocks"]
        self.n_embed = saved_data["n_embed"]
        self.n_heads = saved_data["n_heads"]
        self.n_inner = saved_data["n_inner"]
        self.threshold = saved_data["threshold"]

        self.nn = TransformerNN(
            self.n_features_continuous,
            self.categorical_sizes,
            self.n_classes,
            self.n_embed,
            self.n_inner,
            self.n_blocks,
            self.n_heads,
        )

        # Move to GPU if available
        self.to(self.device)

        # List of features used by the model
        self.features = self.x_names_continuous + self.x_names_categorical

        # Rename the weights keys: remove the "_orig_mod" prefix. This is for loading the compiled models
        weights = {k.replace("_orig_mod.", ""): v for k, v in saved_data["weights"].items()}
        self.load_state_dict(weights)

        # We need to create an alternative map
        mapped = self.map_categorical
        alternative_map = dict()
        for feature, mapping in mapped.items():
            alternative_map[feature] = dict()
            for key, value in mapping.items():
                value = int(value[len(feature) + 1:])
                alternative_map[feature][value] = key

        self.alternative_map = alternative_map

        # Set to eval mode
        self.eval()

    @torch.no_grad()
    def forward(self, x: ak.Array):
        """Forward pass of the NN. Note that the input is an Awkward Array"""
        # Preprocess the root arrays
        x_continuous, x_categorical = self.preprocess(x)

        # Run the network to obtain the logits
        logits = self.nn(x_continuous, x_categorical)

        # Apply the threshold and predict
        probs = logits.softmax(dim=1)
        passed = probs[:, self.signal_idx] >= self.threshold
        probs_ = probs.clone()
        probs_[:, self.signal_idx] = -1
        best_bg = probs_.argmax(dim=1)

        # Return the predictions as numpy arrays
        return torch.where(passed, self.signal_idx, best_bg).cpu().numpy()

    def preprocess(self, x: ak.Array):
        """Preprocess the root arrays"""

        x_continuous = torch.zeros((len(x), self.n_features_continuous), dtype=torch.float32)
        x_categorical = torch.zeros((len(x), len(self.categorical_sizes)), dtype=torch.long)

        # Process continuous features
        for i, feature in enumerate(self.x_names_continuous):
            feature = torch.from_numpy(x[feature].to_numpy())
            feature = (feature - self.mean[i]) / self.std[i]
            x_continuous[:, i] = feature

        # Process categorical features
        for i, feature in enumerate(self.x_names_categorical):
            mapped = self.alternative_map[feature]
            feature = x[feature].to_numpy()

            for key, value in mapped.items():
                feature[feature == key] = value

        x_continuous = x_continuous.to(self.device)
        x_categorical = x_categorical.to(self.device)

        return x_continuous, x_categorical


class TransformerNN(nn.Module):
    def __init__(self,
                 n_features_continuous: int,
                 categorical_sizes: list[int],
                 n_classes: int,
                 n_embed=32,
                 n_inner=None,
                 n_blocks=1,
                 n_heads=1,
                 activation=None,
                 dropout=0.3):
        super().__init__()

        if n_inner is None:
            n_inner = n_embed * 4

        self.activation = nn.GELU(approximate="tanh") if activation is None else activation
        self.embed = MyEmbed(n_features_continuous, categorical_sizes, n_embed)
        self.blocks = nn.Sequential(*[
            Block(n_embed, activation=self.activation, n_inner=n_inner, n_heads=n_heads, dropout=dropout)
            for _ in range(n_blocks)
        ]) if n_blocks > 0 else nn.Identity()
        self.ln = nn.LayerNorm(n_embed)
        self.logits = nn.Linear((n_features_continuous + len(categorical_sizes)) * n_embed, n_classes)

    def forward(self, x_continuous, x_categorical):
        B = x_continuous.shape[0]

        x = self.embed(x_continuous, x_categorical)
        x = self.blocks(x)
        x = self.ln(x)
        x = self.logits(x.view(B, -1))

        return x


class Block(nn.Module):
    def __init__(self, n_embed: int, n_inner=None, n_heads=1, activation=None, dropout=0.0):
        super().__init__()

        if n_inner is None:
            n_inner = n_embed * 4

        if activation is None:
            activation = nn.GELU(approximate="tanh")

        self.attention = Attention(n_embed, n_heads=n_heads, dropout=dropout)

        self.mlp = nn.Sequential(
            nn.Linear(n_embed, n_inner),  # bias is already in the LayerNorm
            activation,
            nn.Linear(n_inner, n_embed),
            nn.Dropout(dropout)
        )

        self.ln1 = nn.LayerNorm(n_embed)
        self.ln2 = nn.LayerNorm(n_embed)

    def forward(self, x):
        x = x + self.attention(self.ln1(x))
        x = x + self.mlp(self.ln2(x))

        return x


class Attention(nn.Module):
    """
    Classic attention module with multi-head support
    """

    def __init__(self, n_embed: int, n_heads=1, dropout=0.0, bias=False):
        super().__init__()

        assert torch.__version__ >= "2.0.0", "This module requires at least torch 2.0.0"
        assert n_embed % n_heads == 0, f"Embedding size ({n_embed}) must be divisible by the number of heads ({n_heads})"

        self.n_heads = n_heads
        self.head_size = n_embed // n_heads
        self.dropout = dropout

        self.linear = nn.Linear(n_embed, 3 * n_embed, bias=bias)  # 3 is to have a single operation for q, k, v

        self.residual_dropout = nn.Dropout(dropout)
        self.residual_project = nn.Linear(n_embed, n_embed, bias=bias)

    def forward(self, x):
        B, T, E = x.shape

        # calculate query, key, values for all heads in batch and move head forward to be the batch dim
        q, k, v = self.linear(x).split(E, dim=2)

        k = k.view(B, T, self.n_heads, self.head_size).transpose(1, 2)
        q = q.view(B, T, self.n_heads, self.head_size).transpose(1, 2)
        v = v.view(B, T, self.n_heads, self.head_size).transpose(1, 2)

        y = torch.nn.functional.scaled_dot_product_attention(q, k, v, dropout_p=self.dropout if self.training else 0)
        y = y.transpose(1, 2).contiguous().view(B, T, E)  # re-assemble all head outputs side by side
        y = self.residual_dropout(self.residual_project(y))

        return y


class MyEmbed(nn.Module):
    def __init__(self, n_features_continuous: int, categorical_sizes: list[int], n_embed: int):
        super().__init__()

        # If we encounter NaN feature, we replace them with a learnable parameter
        self.w_nan = nn.Parameter(torch.randn(n_features_continuous))

        # Separate embedding for each categorical feature
        self.register_buffer("offsets", torch.tensor([0] + categorical_sizes[:-1]).cumsum(dim=0))
        self.w_categorical = nn.Parameter(torch.randn(sum(categorical_sizes), n_embed))
        self.b_categorical = nn.Parameter(torch.randn(len(categorical_sizes), n_embed))

        # Scale and shift for the continuous features
        self.w_continuous = nn.Parameter(torch.randn(n_features_continuous, n_embed))
        self.b_continuous = nn.Parameter(torch.randn(n_features_continuous, n_embed))

    def forward(self, x_continuous, x_categorical):
        # Embed continuous features
        # Replace NaN with a learnable parameter
        x_continuous = torch.where(torch.isnan(x_continuous), self.w_nan, x_continuous)

        # Element-wise scale and shift
        B, F = x_continuous.shape
        x_continuous = x_continuous.view(B, F, 1) * self.w_continuous + self.b_continuous

        # Embed categorical features
        x_categorical = self.w_categorical[x_categorical + self.offsets] + self.b_categorical

        # Concatenate all features
        return torch.cat([x_continuous, x_categorical], dim=1)
