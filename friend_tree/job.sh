#!/bin/bash

# env_dir=/afs/cern.ch/user/v/vyazykov/condor_jobs/myenv
env_dir=/eos/user/v/vyazykov/myenv

echo "env dir: $env_dir"

if [ -d "$env_dir" ]; then
    echo "Virtual environment found!"

    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
    lsetup "root 6.26.08-x86_64-centos7-gcc11-opt"

    source $env_dir/bin/activate
    export PYTHONPATH=$VIRTUAL_ENV/lib64/python3.9/:$ROOTSYS/lib
else
    echo "First run setup_virt_env.sh script in HTCondorScripts directory"
    exit 1
fi


source_base_dir=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_ttWttH/v08/v0801/systematics-full/
target_base_dir="$2"
file="$1"

echo "Source base dir: $source_base_dir"
echo "Target base dir: $target_base_dir"
echo "File: $file"
echo "Python: $(which python) $(python --version)"


BASE_DIR=/afs/cern.ch/user/v/vyazykov/condor_jobs/friend_tree
python $BASE_DIR/add_predictions.py \
        --config $BASE_DIR/addition.yaml \
        --source-base-dir $source_base_dir \
        --target-base-dir $target_base_dir \
        --model-path $BASE_DIR/models/transformer/model.py \
        --branch-name probs_ttH \
        --file $file