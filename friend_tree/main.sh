# ensure env
bash ensure_python_env.sh

# clear logs
rm -rf logs/*
rm -rf /eos/user/v/vyazykov/added_condor

find /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_ttWttH/v08/v0801/systematics-full -name "*.root" > files.txt

mkdir -p logs/out logs/err logs/log

condor_rm -all
condor_submit job.sub